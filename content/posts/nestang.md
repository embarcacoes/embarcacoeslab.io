---
title: "Transformando uma FPGA Tang nano 20k em um Nintendo Entertainment System (NES)"
date: 2023-10-20T11:19:42-03:00
draft: false
categories: ["hardware", "tutoriais"]
tags: ["FPGA", "emulação", "NES"]
authors: ["Julio Nunes Avelar"]
chat_id: "nestang"
---

# Transformando uma FPGA Tang nano 20k em um Nintendo Entertainment System (NES)

![Imagem do MegaMAN no NES](/imgs/posts/nestang/megaman2.jpeg)

## Introdução

Ao abordar o tema de jogar jogos retrô, o uso de emuladores é uma prática bastante comum. Geralmente, quando desejamos emular um jogo, recorremos a algum software de emulação. Atualmente, temos à nossa disposição uma ampla variedade de programas de emulação, muitos dos quais são de código aberto. No entanto, mesmo com a qualidade de muitos emuladores de software, eles costumam apresentar problemas de desempenho, como alta latência e questões de sincronização.

Felizmente, a emulação por hardware está se tornando cada vez mais viável graças às FPGAs (Field Programmable Gate Arrays). Para aqueles que não estão familiarizados, uma FPGA é um dispositivo lógico que pode ser programado para simular um circuito digital. Com uma FPGA, podemos simular um circuito digital completo, descrevendo seu comportamento por meio de uma linguagem de descrição de hardware.

Agora que sabemos que é possível criar um emulador em nível de hardware, vamos colocar a mão na massa e começar a implementação.

## Conheça a Tang nano 20k

A Tang nano 20k é uma FPGA de baixo custo fabricada pela empresa chinesa Sipeed. Ela possui aproximadamente 18 mil blocos lógicos e inclui uma implementação mantida pela própria Sipeed de um emulador de NES chamado NESTang (Nes Emulator for Tang Primer 20K). O NESTang oferece suporte a HDMI, áudio, controles e ao uso de cartão SD para executar imagens de jogos de NES. Além disso, a Sipeed oferece um kit que inclui a placa, uma protoboard, dois controles de PS2 e os adaptadores para conectar os controles á protoboard.

![Imagem do kit](/imgs/posts/nestang/imagem.jpeg)

## Hardware Necessário para Montar o NES

- 1 Tang Nano 20k ou Tang Primer 20k
- 1 ou 2 joysticks e adaptadores para protoboard
- 1 cartão SD
- 1 protoboard
- 1 monitor
- 1 computador com sistema operacional Linux ou MacOS para gravar o NESTang na FPGA
- Leitor de cartão SD

## Software Necessário para Montar o NES

- Python
- OpenFPGAloader

Para os usuários do Windows, a Sipeed fornece um guia passo a passo em seu site oficial: [link](https://wiki.sipeed.com/hardware/en/tang/tang-nano-20k/example/unbox.html).

## Gravando o Firmware na FPGA

Para gravar o firmware na FPGA, basta fazer o download no site oficial da Sipeed: [link](https://dl.sipeed.com/shareURL/TANG/Nano_20K/7_Nestang/firmware). Se preferir, também é possível sintetizá-lo do zero. Para isso, você precisará da IDE Gowin e do código-fonte disponível no GitHub: [link](https://github.com/sipeed/TangNano-20K-example/tree/main/nestang). Neste tutorial, com o objetivo de simplificar o processo, não abordarei esse passo. Em vez disso, iremos gravar o firmware já pronto fornecido pela Sipeed. Após o download do firmware, acesse a pasta onde o arquivo .fs está localizado usando o terminal com o comando "cd". No meu caso, está na pasta Downloads, então usei "cd Downloads". Antes de gravar o firmware, é necessário obter o utilitário openFPGAloader. A instalação do openFPGAloader pode ser feita usando os comandos apropriados, dependendo da distribuição que você está usando.

**Para distribuições baseadas em Debian ou Ubuntu:**

```bash
sudo apt install openFPGALoader
```

**Para distribuições baseadas em Arch:**

```bash
sudo pacman -S openfpgaloader
```

**Para distribuições Fedora:**

```bash
sudo dnf copr enable mobicarte/openFPGALoader
sudo dnf install openFPGALoader
```

**Para MacOS:**

```bash
brew install openfpgaloader
```

Certifique-se de escolher o comando apropriado de acordo com o sistema operacional que você está usando. Uma vez instalado, você estará pronto para gravar o firmware na memória da FPGA com o comando a seguir:

```bash
openFPGALoader -f -b tangnano20k tang_nano_20k_nestang.fs
```

![Imagem do processo](/imgs/posts/nestang/terminal.png)

Após executar este comando, se tudo ocorrer bem, o firmware estará gravado na FPGA. Se você a conectar a um monitor, verá uma imagem como esta:

![Imagem da FPGA](/imgs/posts/nestang/boot_sem_imagem.jpeg)

Não se preocupe se ela estiver assim, pois ainda não possui nenhuma imagem para rodar. Para gravar uma imagem, você precisará de um cartão SD e alguns scripts para gerar a imagem. Se você baixou o código-fonte do NESTang, encontrará uma pasta chamada "tools" com todos esses scripts. Para quem baixou apenas o firmware, esses scripts também estão disponíveis para download [aqui](https://dl.sipeed.com/shareURL/TANG/Nano_20K/7_Nestang/script). Após o download dos scripts e das imagens de NES, você pode gravá-los no cartão usando os seguintes comandos:

## Preparando as Imagens de Jogo

Para gerar a imagem, use:

```bash
python nes2img.py -o games.img img1.nes img2.nes img3.nes … imgN.nes
```

E para gravá-la no cartão SD, utilize:

```bash
sudo dd if=games.img of=/dev/unidade_sd bs=512
```

Substitua "unidade_sd" pelo nome do cartão. Se você estiver utilizando um adaptador USB, provavelmente será algo como sda ou sdb. Se estiver utilizando um adaptador SD ou conectando o cartão diretamente, pode ser algo como mmcblk0.

## Montando o Hardware

Após gravar a imagem, conecte o cartão SD à FPGA.

![Imagem do NES](/imgs/posts/nestang/fpga_sd.jpeg)

Em seguida, conecte os adaptadores em paralelo com os pinos da FPGA, começando pelo lado do HDMI.

![Imagem do adaptador](/imgs/posts/nestang/montagem_adaptador.jpeg)

Após fazer isso, o NES estará pronto. Basta conectar os controles, o HDMI e alimentar a placa. Se tudo correu bem, você verá uma imagem como a mostrada abaixo:

![Imagem do NES](/imgs/posts/nestang/snes_boot.jpeg)

## Jogando no NES

Após iniciar o NES, use os D-pads para escolher o jogo e o botão número 2 para iniciar o jogo. Para retornar ao menu, basta apertar o botão S1 na placa. Depois deste guia passo a passo, é só começar a jogar e se divertir! 😊

## Perguntas e Comentários

Se surgirem dúvidas, sinta-se à vontade para perguntar na seção de comentários abaixo.
