---
title: "Escolhendo microcontroladores para dispositivos IoT e Sistemas embarcados"
date: 2023-04-30T15:27:07-03:00
draft: false
categories: ["hardware", "microcontroladore", "embarcados"]
tags: ["esp32", "eletronica"]
authors: ["Julio Nunes Avelar"]
---

# Escolhendo microcontroladores para dispositivos IoT e Sistemas embarcados

![Microcontroladores](/imgs/posts/microcontroladores/microcontroladores.jpg)

## Sumário:

Aqui está um pequeno guia para ajudá-lo a encontrar os tópicos mencionados neste post:

- [Introdução](#introdução)
  - [Internet das Coisas (IoT)](#internet-das-coisas-iot)
  - [Sistemas embarcados](#sistemas-embarcados)
  - [Dispositivos IoT](#sistemas-embarcados)
  - [Microcontroladores](#microcontroladores)
- [Escolhendo o microcontrolador ideal](#escolhendo-o-microcontrolador-ideal)
  - [Capacidade de processamento](#capacidade-de-processamento)
  - [Memória Volátil](#memória-volátil)
  - [Memória não Volátil](#memória-não-volátil)
  - [Interconectividade](#interconectividade)
  - [Periféricos](#periféricos)
  - [Eficiência energetica](#eficiência-energetica)
  - [Seguança](#segurança)
  - [Custo](#custo)
- [Comparando alguns microcontroladores](#comparando-alguns-microcontroladores)

## Introdução

Antes de tudo, vamos definir alguns conceitos importantes.

### Internet das Coisas (IoT)

Primeiramente, vamos entender o que é a Internet das Coisas (IoT). Não existe um
consenso geral sobre a definição do termo, mas podemos compreendê-lo como um
conjunto de tecnologias e protocolos que permitem que objetos digitais se
conectem à rede de comunicação e sejam controlados por ela. Esses "objetos" são
dispositivos que possuem recursos de computação, comunicação e controle.

![Internet das coisas](/imgs/posts/microcontroladores/iot.jpg)

### Sistemas embarcados

Outro termo importante é o de "sistemas embarcados". Podemos defini-los como
sistemas eletrônicos dedicados a uma função específica ou conjunto de funções,
projetados para operar em ambientes restritos e muitas vezes hostis, com
limitações de recursos, como memória, processamento e energia. Eles são
incorporados em dispositivos ou equipamentos maiores, como automóveis,
dispositivos médicos, eletrodomésticos, equipamentos de automação industrial e
muitos outros. Os sistemas embarcados geralmente contêm um microcontrolador ou
um processador especializado, bem como memória, interfaces de entrada e saída e
outros componentes necessários para realizar suas funções.

![Sistemas embarcados](/imgs/posts/microcontroladores/embarcados.jpg)

### Dispositivos IoT

Como mencionado anteriormente, os dispositivos IoT são os "objetos" da IoT. Eles
são dispositivos com recursos de computação, comunicação e controle, que
geralmente são construídos com componentes como microcontroladores, módulos de
comunicação sem fio, sensores e atuadores. É importante notar que dispositivos
IoT são uma espécie de sistema embarcado.

![Dispositivos IoT](/imgs/posts/microcontroladores/iot-devices.jpg)

### Microcontroladores

Agora que entendemos os conceitos básicos, vamos falar sobre os
microcontroladores. Os microcontroladores são pequenos computadores integrados
em um único chip (circuito integrado) que contêm uma CPU (Unidade Central de
Processamento), memória e periféricos de entrada e saída.

Mas como escolher o microcontrolador ideal para o seu projeto de dispositivo IoT
ou sistema embarcado? Bem, isso é um desafio e tanto! Felizmente, neste post,
vamos explorar como escolher o microcontrolador ideal para o seu projeto de
dispositivo IoT ou sistema embarcado, levando em conta suas características e os
desafios de segurança e privacidade na IoT. Com as informações certas, é
possível criar soluções tecnológicas eficientes, confiáveis e seguras para todo
tipo de requisito.

## Escolhendo o microcontrolador ideal

Existem diversos fatores que devem ser considerados na hora de escolher um
microcontrolador para o seu projeto. Aqui estão algumas das características mais
importantes:

### Capacidade de processamento

A capacidade de processamento do microcontrolador é importante para determinar o
quão rápido ele pode executar as tarefas necessárias ou quantas tarefas ele pode
executar em paralelo. Se o seu projeto envolve processamento de dados em tempo
real ou tarefas complexas que envolvem várias tasks, é importante escolher um
microcontrolador com uma CPU mais poderosa, dados como número de núcleos e clock
máximo são pontos importantes a se considerar neste quesito.

### Memória Volátil

A memória volátil do microcontrolador é composta por tipos de memórias como a
DRAM (Dynamic Random Access Memory), SRAM (Static Random Access Memory) e PSRAM
(Pseudo-Static Random Access Memory), que são utilizadas pela unidade de CPU
para armazenar informações temporárias durante a execução, a fim de executar
funções específicas. Essas memórias dependem de energia para funcionar, ou seja,
quando o microcontrolador é desligado, todas as informações são perdidas. O
microcontrolador utiliza a memória volátil para armazenar dados que ele precisa
acessar rapidamente, como instruções de programa e variáveis de dados. Isso
permite que o microcontrolador execute tarefas com rapidez e eficiência. Em
resumo, a memória volátil é essencial em um microcontrolador, pois oferece um
espaço de armazenamento temporário e de alta velocidade para dados críticos para
a execução de suas funções. Dispositivos que necessitam de uma grande alocação
de memória para o armazenamento de dados ou execução de funções requerem
microcontroladores com uma quantidade maior deste tipo de memória.

### Memória não Volátil

A Memória não Volátil em um microcontrolador é fundamental para a manutenção de
dados que precisam ser armazenados mesmo quando o sistema não está energizado.
Esses dados incluem programas a serem executados pelo microcontrolador,
informações de configuração, arquivos e outros. É importante notar que, em
geral, esses tipos de memória são mais lentos do que as voláteis. Para
aplicações que exigem o armazenamento de programas grandes ou muitos arquivos
junto ao microcontrolador, a capacidade da memória deve ser considerada. Caso
seja necessário armazenar apenas arquivos, alguns microcontroladores oferecem a
opção de usar memórias externas.

### Interconectividade

Para dispositivos IoT ou sistemas embarcados que precisam de conectividade
(Wi-Fi, Bluetooth, Zigbee, LoRa, etc.), é interessante escolher um
microcontrolador que já tenha recursos de interconectividade embutidos, o que
pode reduzir custos e consumo de recursos.

### Periféricos

Os periféricos, como dispositivos de entrada e saída, portas analógicas e
protocolos de comunicação serial, são recursos úteis para um microcontrolador. É
importante escolher um microcontrolador que forneça nativamente os periféricos
necessários para seu projeto ou que permita a adição de módulos externos.

### Eficiência energetica

Se o seu dispositivo é alimentado por baterias ou painéis solares, a eficiência
energética é crucial para garantir a vida útil da bateria e o bom funcionamento
do sistema. É importante analisar o consumo de energia do microcontrolador tanto
em operação contínua quanto em modo de hibernação.

### Segurança

Um dos grandes desafios da Internet das Coisas atualmente é a segurança. Essa
preocupação abrange desde a camada mais alta até a mais baixa, incluindo os
microcontroladores. Dispositivos IoT armazenam diversos dados sensíveis, como
credenciais de acesso e partes de código fechado. Além disso, com as
atualizações remotas via OTA e a comunicação sem fio, é crucial contar com
recursos como criptografia de flash, security boot e suporte à criptografia em
geral. Dispositivos que requerem um nível mínimo de segurança e que serão
utilizados em ambientes não isolados devem contar com esses recursos
indispensáveis. Portanto, é crucial escolher um microcontrolador que conte com
suporte a esses recursos.

### Custo

Por fim, é importante considerar o custo de aquisição e produção do
microcontrolador, garantindo que ele esteja dentro de seu orçamento e permitindo
a produção do dispositivo em larga escala.

## Comparando alguns microcontroladores

| Nome                | Processador    | Nº de nucleos | Clock  | M. Flash | M. Ram (DRAM ou SRAM) | Conectividade | Periféricos                                                 | GPIOs | ADC                    | PWM | Tensao de operação | Custo   |
| ------------------- | -------------- | ------------- | ------ | -------- | --------------------- | ------------- | ----------------------------------------------------------- | ----- | ---------------------- | --- | ------------------ | ------- |
| Arduino Mega        | ATmega2560     | 1             | 16Mhz  | 256KB    | 8KB                   |               | 1 SPI, 1 I2C, 4 UART                                        | 54    | 16 - 10bits            | 15  | 5v                 | R$76,33 |
| Arduino Uno         | ATmega328      | 1             | 16Mhz  | 16KB     | 2KB                   |               | 1 SPI, 1 I2C, 1 UART                                        | 14    | 6 - 10bits             | 6   | 5v                 | R$26,31 |
| Arduino Zero        | ARM Cortex M0+ | 1             | 48Mhz  | 256KB    | 32KB                  |               | 1 SPI, 1 I2C, 2 UART, 2 I2S                                 | 20    | 5 - 12bits, 1 - 10bits | 12  | 3.3v               | R$59,00 |
| ESP-32              | Xtensa LX6     | 2             | 240Mhz | até 16MB | 520KB                 | WIFI, BLE     | 4 SPI, 2 I2C, 2 I2S, 3 UART, CAN                            | 36    | 18 - 12bits            | 16  | 3.3v               | R$12,82 |
| ESP-32S2            | Xtensa LX7     | 1             | 240Mhz | até 16MB | 320KB                 | WIFI          | 4 SPI, 2 I2C, 2 I2S, 2 UART, CAM, OTG, LCD                  | 43    | 20 - 13bits            | 8   | 3.3v               | R$10,46 |
| ESP-32S3            | Xtensa LX7     | 2             | 240Mhz | até 32MB | 512KB                 | WIFI, BLE     | 4 SPI, 2 I2C, 2 I2S, 3 UART, CAM, OTG, LCD, USB Serial JTag | 45    | 20 - 13bits            | 8   | 3.3v               | R$20,03 |
| ESP-32C3            | 32-bit RiscV   | 1             | 160Mhz | até 16MB | 400KB                 | WIFI, BLE     | 3 SPI, 1 I2C, 1 I2S, 2 UART                                 | 22    | 12 - 12bits            | 6   | 3.3v               | R$10,51 |
| ESP-8266            | Xtensa L106    | 1             | 160Mhz | até 16MB | 160KB                 | WIFI          | 4 SPI, 2 I2C, 2 I2S, 2 UART                                 | 17    | 1 - 10bits             | 8   | 3.3v               | R$07,85 |
| NRF52810            | ARM Cortex-M4  | 1             | 64Mhz  | 192KB    | 24KB                  | BLE           | SPI, I2C, UART, PDM                                         | 32    | 8 - 12bits             | 4   | 3.3v               | R$24,90 |
| Raspberry Pi PICO   | ARM Cortex-M0+ | 2             | 133Mhz | até 16MB | 264KB                 |               | 2 SPI, 2 I2C, 2 UART, CAN, OTG                              | 30    | 3 - 12bits             | 16  | 3.3v               | R$23,28 |
| Raspberry Pi PICO W | ARM Cortex-M0+ | 2             | 133Mhz | até 16MB | 264KB                 | WIFI          | 2 SPI, 2 I2C, 2 UART, CAN, OTG                              | 30    | 3 - 12bits             | 16  | 3.3v               | R$53,40 |

Observação: Os dados nesta tabela foram atualizados na data de publicação deste
texto. Se houver qualquer diferença ou inconsistência, por favor, informe-nos. É
importante notar que os preços dos microcontroladores ESP32XX e ESP8266 levam em
conta apenas o preço do módulo, enquanto que os preços dos outros
microcontroladores incluem a placa de desenvolvimento completa. Isso ocorre
porque os outros microcontroladores não são oferecidos apenas como módulos, mas
sim como uma versão completa com a placa de desenvolvimento ou apenas o circuito
integrado. Além disso, esses preços foram cotados no Aliexpress e podem variar
devido a tributos de importação, dependendo da legislação vigente no momento da
compra.

[Clique aqui para acessar a tabela completa.](https://docs.google.com/spreadsheets/d/1fQaBdGPpOdVy6Vd7BasbjTs7t7Xta5GCtgDcengZ8Jg/edit?usp=sharing)
