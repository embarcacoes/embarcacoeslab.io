---
title: "Expedição 2023"
date: 2023-09-25T20:04:40-03:00
draft: false
tags: ["Expedição"]
categories: ["Institucional"]
authors: ["Julio Nunes Avelar"]
---

# Expedição 2023

![Expedição 2023](/imgs/events/expedicao/expedicao.jpg "Expedição 2023")

Você já imaginou a possibilidade de explorar e aprender uma ampla gama de tópicos relacionados à tecnologia? Se a resposta for sim, o Embarcações tem o prazer de convidar você para participar da Expedição 2023, um evento imperdível que acontecerá sábado dia 7 de outubro, no auditório do IC 3, a partir das 9 horas. E o melhor de tudo: a entrada é gratuita e aberta a todos os interessados!

Confira abaixo o cronograma completo do evento:

| Horário         | Palestras                                     | Oficinas                           |
| --------------- | --------------------------------------------- | ---------------------------------- |
| 9:00 às 10:00   | "Do Elétron ao Fogo"                          |                                    |
| 10:00 às 10:20  | "Um Circuito Brilhante"                       |                                    |
| 10:20 às 11:00  | "Trens, Elétrons e Programação"               |                                    |
| 11:00 às 12:00  | "Da Sua Torradeira ao Espaço com Debian"      |                                    |
| 12:00 às 13:00  | Pausa para um Agradável Café                  | Pausa para um Agradável Café       |
| 13:00 às 13:30  | "Robôs de Combate"                            | "Animação em Pixel Art"            |
| 13:30 às 14:30  | "Estruturas de Dados Avançadas"               | "Animação em Pixel Art"            |
| 14:30 às 15:00  | "Conversando em IOT"                          | "Animação em Pixel Art"            |
| 15:00 às 15:40  | "Como Criar um Pinguim (SO Linux)"            | "Introdução à Prototipagem"        |
| 15:40 às 16:20  | "Introdução Básica à Impressora 3D"           | "Introdução à Prototipagem"        |
| 16:20 às 17:30  | "Uma Viagem na Odisseia da Realidade Virtual" | "Introdução à Prototipagem"        |
| 17:30 em diante | Pausa para um Revigorante Café                | Pausa para um Revigorante Café     |

Venha fazer parte dessa incrível jornada de conhecimento em tecnologia! Garanta sua presença no Expedição 2023 e prepare-se para um dia cheio de descobertas e aprendizado. Nos vemos lá!
