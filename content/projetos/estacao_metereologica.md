---
title: "Desenvolvendo uma estação Meteorológica totalmente Open-source"
date: 2023-08-03T21:38:56-03:00
draft: false
categories: ["hardware", "microcontroladore", "embarcados", "iot"]
tags: ["Clima", "esp32", "eletronica"]
authors: ["Julio Nunes Avelar"]
---

# Desenvolvendo uma estação Meteorológica totalmente Open-source

O projeto tem como objetivo o desenvolvimento de uma Estação Meteorológica automática totalmente open-source para fins didáticos, com capacidade de coletar dados do ambiente, como temperatura e umidade, e transmitir essas informações em tempo real através de alguma rede de comunicação sem fio. A Estação Meteorológica será construída utilizando hardware e softwares open-sources e tem como objetivo desenvolver um dispositivo de fácil acesso e aquisição por meio de um desenvolvimento colaborativo e de baixo custo. Ao longo do desenvolvimento do projeto, serão realizados diversos testes com o objetivo de validar a capacidade da Estação Meteorológica de resistir a ambientes hostis e instáveis, ao mesmo tempo em que fornece dados precisos e confiáveis. Espera-se que, ao final do projeto, a Estação meteorológica esteja pronta para uso em ambiente real, com a possibilidade de utilizar a tecnologia desenvolvida em futuros projetos e pesquisas.

## Introdução

Em uma sociedade onde ter informações sobre o clima é cada vez mais necessário e crucial para a realização de diversas tarefas do dia a dia de diferentes pessoas, o surgimento de técnicas como a agricultura de precisão, a Agricultura 4.0 e as cada vez maiores mudanças climáticas impulsionaram o desenvolvimento de novas tecnologias que auxiliam o monitoramento do ambiente ao nosso redor e do clima de nosso planeta. Uma dessas tecnologias desenvolvidas que se mostrou de grande importância foi a criação de estações meteorológicas automáticas.

Ao contrário das estações meteorológicas convencionais, compostas por dispositivos mecânicos que reagem com a ação do tempo e do ambiente e dependem da supervisão de uma pessoa para coleta pontual dos dados, as estações meteorológicas automáticas utilizam sensores eletrônicos para coletar dados do ambiente e, dependendo de como são construídas, são capazes de operar de forma completamente autônoma, sem a necessidade de intervenção humana após a sua calibração. Todos os dados coletados por elas são transmitidos geralmente por alguma forma de comunicação sem fio, como WiFi, LoRa ou GSM.

Dispositivos como estações meteorológicas automáticas se mostram de grande utilidade, coletando e transmitindo dados do ambiente em tempo real, permitindo que os usuários dessa tecnologia tomem decisões melhores com base em informações mais precisas e recentes. No entanto, nem tudo é perfeito. Essa tecnologia ultimamente vem sendo amplamente utilizada, mas ainda possui um alto custo de aquisição e uma certa complexidade para desenvolvimento e calibração.

Para resolver esses problemas, este projeto visa desenvolver uma estação meteorológica automática capaz de coletar dados precisos do ambiente, de forma totalmente open-source e de baixo custo, permitindo assim que qualquer pessoa possa estudar, modificar, estender e fabricar sua própria estação meteorológica automática.

## Objetivos

### Objetivo Geral:

Desenvolver uma estação meteorológica capaz de coletar dados do ambiente e realizar o envio desses dados de forma automática para um serviço na nuvem.

### Objetivos Específicos:

1. Desenvolver e construir um circuito elétrico que permita o carregamento de baterias e o gerenciamento eficientemente dessas baterias;

2. Realizar a integração de diversos sensores com um microcontrolador, garantindo a coleta precisa e confiável de dados;

3. Estabelecer um sistema de comunicação sem fio capaz de atender os requisitos necessários para operação do dispositivo;

4. Realizar a integração física de todos os módulos e circuitos, garantindo um funcionamento harmonioso e eficiente do dispositivo;

5. Desenvolver uma estrutura adequada para acomodar todos os módulos e circuitos do protótipo, considerando restrições de peso, custo e fabricação;

6. Realizar a calibração dos sensores para obter dados mais precisos e confiáveis;

7. Realizar testes abrangentes de compatibilidade e integração dos componentes, garantindo que todos os sistemas funcionem de forma adequada e sem interferências;

8. Desenvolver software para a comunicação eficiente entre o dispositivo e o local onde os dados serão armazenados e tratados.

9. Desenvolver software para o armazenamento, tratamento e disponibilização dos dados fornecidos por estações meteorológicas.

10. Desenvolver um sistema de atualização via "Over the air"(OTA), para permitir a atualização dos dispositivos de forma remota e sem acesso físico.

Além dos objetivos mencionados acima, é importante considerar a documentação detalhada de todo o processo de desenvolvimento e construção, incluindo relatórios de projeto, manuais de operação e manutenção, bem como a avaliação dos resultados obtidos e possíveis recomendações para melhorias futuras.

## Justificativa

O desenvolvimento da estação meteorológica se justifica pela demanda deste tipo de dispositivo por parte de diversas pessoas e organizações, sejam elas vinculadas a universidades ou não; além disso, todo o conhecimento obtido ao longo do processo de desenvolvimento e os dados que podem vir a ser coletados pelos dispositivos podem ser muito úteis em projetos e pesquisas futuras, com todo o embasamento obtido ao longo do projeto sendo útil para a comunidade acadêmica ao longo de um longo período de tempo; por último, não menos importante, o desenvolvimento de uma estação meteorológica automática, completamente open-source, auxilia na democratização do acesso a tecnologias de grande importância para o desenvolvimento de atividades como a agricultura e pesquisas sobre o clima, além de permitir um desenvolvimento totalmente colaborativo e de baixo custo, permitindo assim que pessoas comuns sem acesso a grandes quantidades de recursos materiais ou humanos estejam contribuindo com o projeto.

## Metodologia

Trata-se de um projeto com viés tecnológico e desenvolvimento de forma exploratória, envolvendo procedimentos experimentais e empíricos. A elaboração do dispositivo ocorrerá mediante a fabricação e testes de diversos protótipos e dispositivos, os quais permitirão a coleta de uma variedade de dados durante essa fase experimental. Essas informações serão posteriormente analisadas para avaliar o desempenho, eficácia e viabilidade de implementação no dispositivo final.

Os protótipos destinados aos testes serão concebidos com base em fundamentação teórica e experiências anteriores no desenvolvimento de dispositivos eletrônicos.

Para realizar o desenvolvimento do projeto, serão adotados os seguintes procedimentos:

1. Aprofundamento dos estudos sobre microcontroladores e arquitetura de computadores;

2. Estudo sobre transmissão de dados através de redes sem fio;

3. Construção de protótipos de hardware e software;

4. Programação de placas de prototipagem, como módulos de desenvolvimento, para realizar testes de integração;

5. Desenvolvimento de uma estrutura física para a acomodação e proteção dos componentes do dispositivo;

6. Integração do hardware desenvolvido à estrutura do dispositivo;

7. Integração do software de todos os módulos do dispositivo;

8. Realização de testes em bancada com o dispositivo desenvolvido;

9. Realização de testes em ambiente controlado para analisar o comportamento do dispositivo em condições específicas;

10. Elaboração de artigos científicos e apresentação de resultados em eventos acadêmicos.

## Resultado esperados

Espera-se que ao final do projeto já se tenha concluído o desenvolvimento da estação meteorológica, com a mesma já estando operando em fase de testes ou produção. Espera-se que todo o conhecimento obtido e todas as tecnologias desenvolvidas ao logo do projeto possam ser utilizados em projetos e pesquisas futuras.

## Orçamento

Durante o decorrer do projeto, alguns equipamentos utilizados serão fornecidos pela estrutura da universidade, enquanto outros já estão disponíveis para o desenvolvedor do projeto. Entretanto, é essencial ressaltar que a aquisição dos componentes requer recursos adicionais, uma vez que muitos deles ainda precisam ser adquiridos.

O desenvolvedor já possui uma parte dos componentes necessários, o que pode reduzir os custos de aquisição. Contudo, é preciso buscar recursos extras para garantir a obtenção de todos os componentes necessários para o desenvolvimento da Estação Meteorológica.

A versão em PDF deste post está disponivel em: [Estação Meteorológica](/docs/Estação_Metereologica.pdf)
