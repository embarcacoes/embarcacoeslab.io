---
title: "Monitoramento inteligente de filas: Uma abordagem utilizando interceptação de pacotes de rede WiFi"
date: 2023-05-24T21:38:56-03:00
draft: false
categories: ["hardware", "microcontroladore", "embarcados", "iot"]
tags: ["Filas", "esp32", "eletronica"]
authors: ["Julio Nunes Avelar"]
---

# Monitoramento inteligente de filas: Uma abordagem utilizando interceptação de pacotes de rede WiFi

Este post descreve a implementação de um sistema de monitoramento inteligente de filas utilizando a interceptação de pacotes de rede WiFi. O objetivo é obter informações precisas sobre a quantidade de pessoas, tempo médio de espera e fluxo na fila, permitindo uma gestão mais eficiente do fluxo de pessoas. A abordagem proposta utiliza a interceptação física dos pacotes em uma frequência de rádio de 2.4GHz, utilizando o microcontrolador ESP32 com seu módulo WiFi embutido. Através desse sistema, é possível obter dados dos dispositivos móveis conectados à rede ou mesmo dos dispositivos sem conexão com nenhuma rede. O monitoramento inteligente de filas proporciona uma melhor experiência para os usuários e permite uma tomada de decisão mais embasada na gestão de fluxo em ambientes com filas de espera. Além disso, discutimos a arquitetura do sistema, a questão da privacidade dos usuários e possíveis desafios e limitações da implementação.

## Introdução

No cotidiano das pessoas, é extremamente comum o uso de filas para controlar o acesso a algum serviço ou recurso. Em ambientes com múltiplos usuários, é frequente a formação de filas de espera como forma de controlar o acesso. As filas são algo extremamente simples e intuitivo, porém, à medida que crescem em proporção, torna-se complexo obter um diagnóstico preciso, seja em relação à quantidade de pessoas presentes na fila, ao tempo médio de espera de cada pessoa ou ao fluxo de pessoas na fila. Para resolver esse problema, surgiram diversas soluções tecnológicas, e uma delas é a contagem dos indivíduos na fila e a determinação de suas posições através de seus dispositivos móveis.

## Implementação

### Pacotes de dados

Quando dispositivos realizam comunicação, seja por uma rede com ou sem fio, eles enviam informações através de pacotes, nos quais são incluídas todas as informações necessárias para que o pacote chegue de um dispositivo a outro, além dos dados propriamente ditos. Por questões de segurança, geralmente os dados são criptografados, mas ainda assim é possível identificar quem enviou o pacote e qual é o destinatário.

### Interceptação de pacotes

Existem duas maneiras principais de interceptar pacotes: via hardware ou via software. A implementação via software consiste em um programa que analisa os pacotes de uma determinada rede e coleta informações, enquanto a implementação via hardware consiste na interceptação física dos pacotes, seja analisando os dados através de cabeamento ou em uma determinada frequência de rádio. No caso deste sistema, optou-se pela implementação via hardware, utilizando a interceptação física dos pacotes de rede WiFi na frequência de rádio de 2.4GHz.

### O hardware utilizado

Para implementar o sistema de monitoramento, foi escolhido o microcontrolador ESP32 (ESP32, ESP32S2, ESP32C3 ou ESP32S3), que possui um módulo WiFi embutido. Essa escolha reduz o custo e facilita a implementação do sistema. Além disso, o framework de desenvolvimento do ESP32, ESP-IDF, fornece controle total sobre o WiFi, permitindo a utilização do microcontrolador em um modo no qual ele pode fornecer os dados recebidos pelo módulo WiFi.

### Arquitetura do sistema

O sistema de monitoramento de filas consiste em diversas camadas que processam e disponibilizam os dados coletados.

#### Coleta dos dados

Um microcontrolador ESP32 é responsável por interceptar e analisar superficialmente os pacotes, obtendo o endereço MAC do dispositivo responsável pelo envio do pacote e a intensidade do sinal recebido (RSSI).

#### Envio dos dados

Os dados coletados são enviados para um micro-serviço na nuvem por meio de um segundo ESP32 conectado ao primeiro via serial. O protocolo UART é utilizado para transmitir as informações coletadas e encaminhá-las para a nuvem por meio do protocolo MQTT.

#### Tratamento e disponibilização dos dados

Na nuvem, um micro-serviço recebe os dados enviados pelos ESP32s e os processa. Utilizando os endereços MAC, a distância aproximada dos dispositivos com base no RSSI e os dispositivos em que esses endereços MAC foram observados, é possível estimar a quantidade de pessoas na fila e suas posições aproximadas.

### Múltiplos dispositivos para cobrir uma fila

Devido ao comprimento considerável da fila, vários dispositivos ESP32 são utilizados para monitorar toda a extensão da mesma. Através de uma malha formada por esses dispositivos, é possível estimar, de forma aproximada, a posição de um determinado dispositivo e monitorar seu deslocamento.

## Privacidade

Um aspecto importante ao interceptar pacotes de rede é a privacidade das pessoas no ambiente monitorado. Dispositivos móveis que não estão conectados a uma rede WiFi ainda enviam periodicamente pacotes de dados, mesmo com o WiFi desligado e o modo avião desligado. Para abordar essa questão, sistemas operacionais como Android e iOS utilizam endereços MAC aleatórios em certos períodos de tempo, a fim de anonimizar os dispositivos em relação aos sniffers. Além disso, todo o código-fonte do sistema é de código aberto, permitindo que os afetados por essa tecnologia verifiquem se nenhuma atividade maliciosa está sendo realizada.

## Conclusão

O monitoramento inteligente de filas utilizando a interceptação de pacotes de rede WiFi é uma abordagem que permite contar e estimar a posição de quase todas as pessoas em uma fila. A implementação é relativamente simples e não viola a privacidade das pessoas por meio do uso de imagens. No entanto, o sistema enfrenta desafios, como a dificuldade de encontrar hardwares simples compatíveis com redes WiFi de 5GHz, dispositivos móveis que passam despercebidos pelo sniffer quando estão no modo avião e a contabilização de dispositivos em um raio próximo à fila.

Considerando esses pontos, o sistema de monitoramento inteligente de filas oferece uma solução eficiente para a gestão de fluxo em ambientes com filas de espera, proporcionando uma melhor experiência para os usuários e embasando a tomada de decisões na gestão do fluxo.

Por ultimo, ao longo do desenvolvimento do projeto surgiu-se 2 repositórios com todo o código fonte:
[Interceptador - Sniffer](https://gitlab.com/embarcacoes/sniffer-node) ,
[Transmissor](https://gitlab.com/embarcacoes/sniffer-sender)

A versão em PDF deste post está disponivel em: [Sniffer](/docs/Monitoramento_inteligente_de_filas.pdf)
