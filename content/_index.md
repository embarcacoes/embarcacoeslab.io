---
title: "Home"
date: 2023-04-27
draft: false
---

{{< most-recent-box "posts" "Confira a última postagem de nosso blog!" >}}


# Bem-vindo ao Embarcações: Explorando o Universo dos Sistemas Embarcados

O Embarcações é um grupo de estudos em Hardware focado em sistemas embarcados.
Somos abertos a qualquer pessoa interessada, sem nenhum requisito prévio. Basta
comparecer a uma de nossas reuniões semanais.

# Quem somos

Somos um grupo de alunos da UNICAMP apaixonados por tecnologia, com ampla
experiência em eletrônica, programação, sistemas embarcados e IoT. Nosso
objetivo é compartilhar conhecimento, desenvolver projetos open-source e ajudar
tanto a comunidade interna da UNICAMP quanto a comunidade externa, oferecendo
soluções que facilitem o dia a dia de todos.

# Objetivos

Nossos principais objetivos são:

- Disseminar conhecimento sobre Eletrônica, Sistemas Embarcados, IoT,
  programação e outros temas relacionados à Tecnologia em geral.
- Desenvolver e divulgar projetos open-source em todas as áreas mencionadas
  acima.
- Ajudar as pessoas a aprender novos conteúdos e resolver problemas cotidianos
  relacionados à tecnologia.
- Contribuir com a comunidade da UNICAMP desenvolvendo soluções para problemas
  internos.

{{< alerts warning
"**Nota**: O Embarcações surgiu no ambiente da Unicamp, e por isso existe bastante interação com seus alunos, porém não é restrito a esse ambiente de forma alguma. Todas as pessoas são mais que bem-vindas a participar."
>}}

# Contribua Conosco

Todos os membros do Embarcações são voluntários, e valorizamos todas as
contribuições. Nosso site e todos os nossos projetos são open-source e estão
disponíveis no GitLab, proporcionando acesso aberto e colaborativo.
